/**
 * @file snap_apm_test.cpp
 * @brief CPU entry point for querying Snapdragon APM
 * @author Parker Lusk <plusk@mit.edu>
 * @author Aleix Paris <aleix@mit.edu>
 * @date 28 July 2019
 */

#include <iostream>

#include "snap_apm/snap_apm.h"


int main(int argc, char const *argv[])
{

    acl::SnapAPM apm;
    apm.init();

    float voltage, current;
    apm.read(voltage, current);
    std::cout << "snap_apm_test  V: " << voltage << ", I: " << current << std::endl;

    apm.close();

    return 0;
}
