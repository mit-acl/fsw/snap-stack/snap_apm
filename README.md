Snapdragon Advanced Power Module (APM) Interface
================================================

Based on code snippet posted on [QDN forum](https://developer.qualcomm.com/forum/qdn-forums/hardware/snapdragon-flight/34211).

## Building

1. Clone into the [`sfpro-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev) or [`sf-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/snap_apm --load`.

Note: the LTC2946 has to be accessed through two different addresses to obtain the APM's input voltage (battery voltage) or to obtain the APM's output voltage (5V). These addresses are hardcoded in `apm_imp.c`.

## Example Output

### snap_apm_test

Run `./snap_apm_test` to see the voltage and current on the terminal:

```
snap_apm_test  V: 16.379, I: 0
```

### mini-dm

The following example output was obtained via `mini-dm` (i.e., `make mini-dm` from [sf-dev](https://github.com/mit-acl/fsw/snap-stack/sf-dev) or [sfpro-dev](https://github.com/mit-acl/fsw/snap-stack/sfpro-dev)) on an Eagle 8074 while plugged in to the wall via the APM.

```bash
[08500/02]  08:17.160  HAP:364604:[ltc2949] Successfully opened '/dev/iic-3' and configured for slave 0x6b.  0056  ltc2946.c
[08500/02]  08:17.160  HAP:364604:[apm] Successfully initialized.  0029  apm_imp.c
[08500/02]  08:17.161  HAP:364604:[apm] V: 5.15 I: 0.32 t: 4097161723  0046  apm_imp.c
[08500/02]  08:17.161  HAP:364604:[ltc2946] Deinitialized.  0073  ltc2946.c
[08500/02]  08:17.161  HAP:364604:[apm] Closed.  0057  apm_imp.c
```

