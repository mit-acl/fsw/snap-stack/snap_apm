/**
 * @file snap_apm.h
 * @brief Snap APM API
 * @author Aleix Paris <aleix@mit.edu>
 * @date 17 February 2020
 */

#pragma once

#include <cstdint>

namespace acl {

class SnapAPM
{
public:
    SnapAPM(){}
    ~SnapAPM();

    bool init();
    void close();
    bool read(float& voltage, float& current);
    bool hw_error() const { return hw_error_; }

private:
    bool initialized_ = false; ///< apm peripheral initialized (needs closing)
    bool hw_error_ = false; ///< cannot communicate with device
};

} // ns acl
