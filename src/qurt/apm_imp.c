/**
 * @file apm_imp.c
 * @brief DSP implementation of APM sensor using LTC2946
 * @author Parker Lusk <plusk@mit.edu>
 * @author Aleix Paris <aleix@mit.edu>
 * @date 28 July 2019
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "ltc2946.h"

#define LTC2946_I2C_ADDRESS 0b1101010   // I2C address to measure battery supply voltage and current
//#define LTC2946_I2C_ADDRESS 0b1101011   // I2C address to measure 5V supply voltage and current

// ----------------------------------------------------------------------------

int apm_init(void)
{
  // Open /dev/iic-# (note: using BLSP). This is I2C3 (J14) on APQ8074
  const uint8_t i2c_devnum = I2C_DEVNUM;  // 3 if QC_SOC_TARGET=APQ8074, 8 if QC_SOC_TARGET=APQ8096
  bool success = ltc2946_init(i2c_devnum, LTC2946_I2C_ADDRESS);
  if (!success) return APM_ERROR;

  LOG_INFO("[apm] Successfully initialized.");

  // i2c_sniff(3);

  return APM_SUCCESS;
}

// ----------------------------------------------------------------------------

int apm_read(float* voltage, float* current)
{
  uint64_t time_us;
  ltc2946_read(voltage, current, &time_us);

  //LOG_INFO("[apm] V: %.2f\tI: %.2f\tt: %llu", *voltage, *current, time_us);

  return APM_SUCCESS;
}

// ----------------------------------------------------------------------------

void apm_close(void)
{
  ltc2946_deinit();

  LOG_INFO("[apm] Closed.");
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static void i2c_sniff(int i2cdevnum)
{

  // open I2C device
  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  int fd = open(dev, O_RDWR);

  for (uint8_t i=0; i<10; ++i) {

    // Configure I2C with slave address
    struct dspal_i2c_ioctl_slave_config slave_config;
    slave_config.slave_address = i;
    slave_config.bus_frequency_in_khz = 400;
    slave_config.byte_transer_timeout_in_usecs = 9000;
    ioctl(fd, I2C_IOCTL_CONFIG, &slave_config);

    // query for device on this bus with addr i
    uint8_t data = 0;
    uint8_t len = write(fd, &data, 1);
    LOG_INFO("0x%x: Sent %d bytes", i, len);

    usleep(100000);
  }

  // close I2C device
  close(fd);

}
