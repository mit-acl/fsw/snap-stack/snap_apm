/**
 * @file ltc2946.c
 * @brief C API for the LTC2949 I2C Power Monitor (on Snapdragon APM)
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 July 2019
 */

#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <dspal_time.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "ltc2946.h"

static int fd_ = -1;
static uint8_t addr_ = 0x00;

static int _configure_sensor();
static bool _i2c_slave_config(uint8_t addr);
static bool _write8(uint8_t reg, uint8_t data);
static bool _write(uint8_t * data, uint8_t len);
static uint8_t _read16(uint8_t reg, uint8_t * buf);

// ----------------------------------------------------------------------------

bool ltc2946_init(uint8_t i2cdevnum, uint8_t addr)
{
  // 
  // Open the requested I2C device
  //

  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  fd_ = open(dev, O_RDWR);

  if (fd_ == -1) {
    LOG_ERR("[ltc2949] Opening '%s' failed.", dev);
    return false;
  }

  //
  // Configure the I2C device and sensor
  //

  addr_ = addr;

  bool ret = _i2c_slave_config(addr);
  if (!ret) return false;

  _configure_sensor();

  LOG_INFO("[ltc2949] Successfully opened '%s' "
           "and configured for slave 0x%x.", dev, addr);

  return true;
}

// ----------------------------------------------------------------------------

void ltc2946_deinit()
{
  if (fd_ == -1) return;

  // close the file descriptor
  close(fd_);

  // indicate that device is closed
  fd_ = -1;

  LOG_INFO("[ltc2946] Deinitialized.");
}

// ----------------------------------------------------------------------------
 
bool ltc2946_read(float * voltage, float * current, uint64_t * time_us)
{
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  *time_us = tp.tv_sec * 1e6 + tp.tv_nsec * 1e-3;

  uint8_t vraw[2];
  uint8_t iraw[2];

  int vread = _read16(0x1E, vraw);
  int iread = _read16(0x14, iraw);

  // int volt_read_ret = i2c_read_reg(file_des,0x1E,vraw,2);    //read raw voltage measurement from 0x1E register (2 bytes)
  // int curr_read_ret = i2c_read_reg(file_des,0x14,iraw,2);    //read raw current measurement from 0x14 register (2 bytes)

  if (vread!=2 || iread!=2) return false;

  uint16_t volt16 = (((uint16_t)vraw[0]) << 8) | vraw[1];  //MSB first
  volt16        >>= 4;                                     //data is 12 bit and left-aligned
  *voltage        = volt16/4095.0 * 102.4;                 //102.4V is maximum voltage on this input

  uint16_t curr16 = (((uint16_t)iraw[0]) << 8) | iraw[1];  //MSB first
  curr16        >>= 4;                                     //data is 12 bit and left-aligned

  // float r_sense  = 0.001;                               //current sense resistor value on Eagle ESC
  // float r_sense  = 0.0005;                              //current sense resistor value on Eagle APM (main current sense)
  float r_sense  = 0.005;                                  //current sense resistor value on Eagle APM (5V current sense)
  *current       = curr16/4095.0 * 0.1024 / r_sense;       //0.1024V is maximum voltage on this input, 0.001 Ohm resistor

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static int _configure_sensor()
{
  uint8_t CTRLA = 0b01001000;   //offset calib every 128 conv, so sampling takes about 35ms (for both voltage and current together)
  //uint8_t CTRLA = 0b00001000; //Gnd ref, offset evey conv, volt=sense+, alternate volt and curr. if use this setting, it takes about 100ms to sample
  uint8_t CTRLB = 0b00000100;

  // resolution for voltage sensing is 25mV/LSB (not very high... but voltage range is high)

  _write8(0x00, CTRLA);
  _write8(0x01, CTRLB);
 
  return 0;
}

// ----------------------------------------------------------------------------

static bool _i2c_slave_config(uint8_t addr)
{
  struct dspal_i2c_ioctl_slave_config slave_config;
  slave_config.slave_address = addr;
  slave_config.bus_frequency_in_khz = 400;
  slave_config.byte_transer_timeout_in_usecs = 9000;

  bool ret = ioctl(fd_, I2C_IOCTL_CONFIG, &slave_config);

  if (ret != 0) {
    LOG_ERR("[ltc2949] IOCTL slave 0x%x failed.", addr);
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

static bool _write8(uint8_t reg, uint8_t data)
{
  uint8_t buf[2] = { reg, data };

  return _write(buf, 2);
}

// ----------------------------------------------------------------------------

static bool _write(uint8_t * data, uint8_t len)
{
  uint8_t byte_count = write(fd_, data, len);

  if (byte_count != len) {
    LOG_ERR("[ltc2949] Expected %d bytes written, sent %d", len, byte_count);
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

static uint8_t _read16(uint8_t reg, uint8_t * buf)
{
  struct dspal_i2c_ioctl_combined_write_read ioctl_write_read;
  ioctl_write_read.write_buf     = &reg;
  ioctl_write_read.write_buf_len = 1;
  ioctl_write_read.read_buf      = buf;
  ioctl_write_read.read_buf_len  = 2;
  uint8_t byte_count = ioctl(fd_, I2C_IOCTL_RDWR, &ioctl_write_read);

  if (byte_count != 2)
    LOG_ERR("[ltc2949] Expected 2 byte received, got %d", byte_count);

  return byte_count;
}
