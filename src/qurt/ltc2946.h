/**
 * @file ltc2946.h
 * @brief C API for the LTC2949 I2C Power Monitor (on Snapdragon APM)
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 July 2019
 */

#pragma once

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

bool ltc2946_init(uint8_t i2cdevnum, uint8_t addr);
void ltc2946_deinit();
bool ltc2946_read(float * voltage, float * current, uint64_t * time_us);
