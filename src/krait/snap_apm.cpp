/**
 * @file snap_apm.cpp
 * @brief Snap APM API
 * @author Aleix Paris <aleix@mit.edu>
 * @date 17 February 2020
 */

#include "snap_apm/snap_apm.h"

#include "utils.h"
#include "apm.h"

namespace acl {

SnapAPM::~SnapAPM()
{
    if (initialized_) close();
}

// ----------------------------------------------------------------------------

bool SnapAPM::init()
{
    // Make sure we can connect to the device
    if (apm_init() != APM_SUCCESS) {
        LOG_ERR("Hardware error: cannot initialize DSP-side peripheral.");
        hw_error_ = true;
        return false;
    }

    hw_error_ = false;
    initialized_ = true;
    return true;
}

// ----------------------------------------------------------------------------

void SnapAPM::close()
{
    apm_close();
    initialized_ = false;
}

// ----------------------------------------------------------------------------

bool SnapAPM::read(float& voltage, float& current)
{
    apm_read(&voltage, &current);
    return true;  // TODO: return false if it couldn't be read
}

} // ns acl
